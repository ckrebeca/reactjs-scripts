  
import React from 'react'
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom'
import Page from './Components/dynaPage'
import NotFound from './Components/NotFound'

class NewRecord extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            steps: [
                {
                  id:'step_1',
                  content: 'This is STEP 1 Content!',
                  next:'step_2',
                  previous: null
                },
                {
                  id:'step_2',
                  content: 'This is STEP 2 Content!',
                  next:'step_3',
                  previous: 'step_1'

                },
                {
                  id:'step_3',
                  content: 'This is STEP 3 Content!',
                  next:'step_4',
                  previous: 'step_2'
                },
                {
                    id:'step_4',
                    content: 'Completed. Thank you!!',
                    next: null,
                    previous: 'step_3'
                }
              ]
        }
    }


  renderSteps = (routerProps) => {
    let stepId = routerProps.match.params.id
    let currentStep = this.state.steps.find(stepObj =>stepObj.id === stepId)
    return (currentStep ? <Page step={currentStep}/> : <NotFound/>)
  }

  render(){
    return(
    <div className="App">
        <h1>Record Creation</h1>
        <BrowserRouter>

        <Link to = {`/new_record/step_1`}> Start </Link> <br/>

        <Switch>
            <Route exact path = '/new_record'/>   
            <Route path = '/new_record/:id' render = {routerProps => this.renderSteps(routerProps)} />
            <Route component = {NotFound} />
        </Switch>
        
        </BrowserRouter>

    </div>
  )}
}

export default NewRecord;