  
import React from 'react'
import {BrowserRouter, Route, Switch, Link} from 'react-router-dom'
import Home from './Components/Home'
import NewRecord from './new_record'
import NotFound from './Components/NotFound'

class Record extends React.Component {

  render(){
    return(
    <div className="App">
        <BrowserRouter>
        <Link to='/'> Home </Link>
        <br/>
        <Link to='/new_record'> New Record </Link>
        <Switch>
            <Route exact path = '/' component = {Home} />
            <Route exact path = '/new_record' component = {NewRecord} />
            <Route component = {NotFound} />
        </Switch>
        </BrowserRouter>
    </div>
  )}
}

export default Record;