import React from 'react'
import {Link} from 'react-router-dom'

const Page = ({step}) => {
    return (
        <div>
            {step.content}
            <br/>
            <Link to = {`/new_record/${step.next}`}> Next </Link> <br/>
            <Link to = {`/new_record/${step.previous}`}> Previous </Link> <br/>
        </div>
    )
}

export default Page
