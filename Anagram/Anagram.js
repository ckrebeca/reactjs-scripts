import React from 'react'

class EventHandler extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            first_string: "",
            second_string:"",
            result: ""
        }
    }

    verifyAnagram(){
       var firstStringAlpha = this.state.first_string.toLowerCase().split("").sort().join("");
       var secStringAlpha = this.state.second_string.toLowerCase().split("").sort().join("");
       console.log(firstStringAlpha, secStringAlpha)
       if (firstStringAlpha === secStringAlpha){
           return true
        }
        else{
            return false
        }
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        })
}

    render(){
        const isAnagram = this.verifyAnagram()
        return (
        <div>
        <label>
          First Word:
          <input name="first_string" type="text" onChange={ (event) => this.handleChange(event)} />
        </label>
            <br/>
        <label>
          Second Word:
          <input name="second_string" type="text" onChange={ (event) => this.handleChange(event)} />
        </label>
        {
            isAnagram ?
            <div>
                <h1>Anagram</h1>
            </div>
            :
            <div>
                <h1>Not Anagram</h1>
            </div>
        }
        </div>
        )
    }
}

export default EventHandler