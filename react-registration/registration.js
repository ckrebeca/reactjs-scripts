import React from 'react'
import Finder from './finderForm'
import Result from './results'

const schools = [
    {
        schoolName: "Great Oak High",
        maxAge: 7,
        aircon: true,
        parentControl: true
    },
    {
        schoolName: "Clearwater Academy",
        maxAge: 9,
        aircon: false,
        parentControl: true
    },
    {
        schoolName: "Lakeside Grammar School",
        maxAge: 15,
        aircon: true,
        parentControl: true
    },
    {
        schoolName:"Skyline Middle School",
        maxAge: 18,
        aircon: true,
        parentControl: false
    },

]
class Registration extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            age: 0,
            needsAircon: false,
            fullName: "Cy",
            parentControl: false
        }
    }
    handleFullNameChange = (e) => {
        this.setState({ fullName: e })
    }

    handleAgeChange = (e) => {
        this.setState({ age: e })
    }

    handleAirconChange = (e) => {
        this.setState({ needsAircon: e })
    }

    handleParentControlChange = (e) => {
        this.setState({parentControl: e})
    }

    filterSchools = () => {
        return schools.filter(e => {
            return (this.state.age <= e.maxAge && 
                    this.state.needsAircon === e.aircon &&
                    this.state.parentControl === e.parentControl)

        })
    }
    render() {
        return (
            <div>
                <Finder
                    onFullnameChange={this.handleFullNameChange}
                    onAgeChange={this.handleAgeChange}
                    onAirconChange={this.handleAirconChange}
                    onParentControlChange={this.handleParentControlChange}

                    info={this.state} />
                <Result schools={this.filterSchools()} />
            </div>
        )
    }
}
export default Registration;