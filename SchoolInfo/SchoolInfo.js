import React from 'react'

class School extends React.Component{
    
    constructor(props){
        super(props)
        this.state = {
            schoolName:"Great Oak High",
            grade: "",
            numSchoolClasses:"",
            numStudents:"",
            schoolAircon:"Yes",
            totalData: JSON.stringify(localStorage)
        }
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit() {
        // alert(localStorage.length + 1) // therefore counting starts at 1, not 0
        var finalData = {
                            "schoolName": this.state.schoolName,
                            "grade": this.state.grade, 
                            "numSchoolClasses": this.state.numSchoolClasses,
                            "numStudents": this.state.numStudents,
                            "schoolAircon": this.state.schoolAircon,
                        };
        localStorage.setItem(localStorage.length+1, JSON.stringify(finalData))
    }


    returnTableRows(){
        var rows = [];
        for (var i = 1; i < localStorage.length + 1; i++) {
           rows.push(this.generateRowData(i));
        }
        return rows;
    }

    generateRowData(i){
        return(
            <tr key={i}>
            <td>{(JSON.parse(localStorage.getItem(i)).schoolName)}</td>
            <td>{(JSON.parse(localStorage.getItem(i)).grade)}</td>
            <td>{(JSON.parse(localStorage.getItem(i)).numSchoolClasses)}</td>
            <td>{(JSON.parse(localStorage.getItem(i)).numStudents)}</td>
            <td>{(JSON.parse(localStorage.getItem(i)).schoolAircon)}</td>
            </tr>
        )
    }

    render(){
        return (
      
        <div style={{textAlign:"center"}}>
            <form onChange={ (event) => this.handleChange(event)} onSubmit={ (event) => this.handleSubmit(event)}>
                <hr/>
                <label>
                Select School:
                <br/>
                <select name="schoolName">
                    <option selected value="Great Oak High">Great Oak High</option>
                    <option value="Clearwater Academy">Clearwater Academy</option>
                    <option value="Lakeside Grammar School">Lakeside Grammar School</option>
                    <option value="Skyline Middle School">Skyline Middle School</option>
                </select>
                </label>

                <br/>

                <label>
                Grade: 
                <br/>
                <input name="grade" required/>
                </label>

                <br/>

                <label>
                No. of Classes: 
                <br/>
                <input name="numSchoolClasses" required type="number" min='1' max='100'/>
                </label>

                <br/>

                <label>
                No. of students per class: 
                <br/>
                <input name="numStudents" required type="number" min='1' max='100'/>
                </label>

                <br/>

                <label>Are there any air-conditioners installed?</label>
                <br/>
                <select name="schoolAircon">
                    <option selected value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>

                <br/>
                <br/>

                <input type="submit" value="Submit" />
                <hr/>
            </form>
            <table>
                <tbody>
                    <tr>
                        <th> School </th>
                        <th> Grade </th>
                        <th> No. of Classes </th>
                        <th> No. of Students per Classes </th>
                        <th> Aircon Availability </th>
                    </tr>
                    {this.returnTableRows()}
                </tbody>
            </table>

        </div>
        )
    }
}

export default School